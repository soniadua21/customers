const express = require('express');
const router = express.Router();
const Customer = require('../models/customers.models');
var fs = require("fs");
// for inserting data at initial
exports.allData = async function (req, res, next) {
    fs.readFile("customers.txt", "utf-8", (err, data) => {

        if (err) { console.log(err) }
        var jsonstring = data.split('\n')
        var jsonparse = jsonstring.map(JSON.parse)
       
        for (let cust of jsonparse) {
            let user = {
                "user_id": cust.user_id,
                "name": cust.name,
                "location": {
                    "type": "Point",
                    "coordinates": [+cust.longitude, +cust.latitude]
                }
            }
            let detail = new Customer(user)
            insertAll(detail).then((data) => {
                console.log(data)
                res.json(data)
            });
        }

    })
}
function insertAll(detail) {
    var promises = [
        new Promise(function (resolve, reject) {
            detail.save();

        })
    ];
    return Promise.all(promises);
}
// query for finding the customer nearby dublin
exports.filterData = function (req, res, next) {
    var pipeline = [{
        $geoNear: {
            near: { type: "Point", coordinates: [-6.257664, 53.339428] },
            distanceField: "dist.calculated",
            maxDistance: 100000,
            distanceMultiplier: 0.001,
            includeLocs: "dist.location",
            spherical: true
        }
    },
    {
        $project: { user_id: 1, name: 1, _id: 0 }
    },
    {
        $sort: { user_id: 1 }
    }
    ]
    filterPipeData(pipeline).then((data) => {
        console.log(data)
        res.json(data)
    });


}

function filterPipeData(pipeline) {

    return new Promise((resolve, reject) => {
        if (resolve) {
            let data = Customer.aggregate(pipeline);

            resolve(data)
        }
    });
}

