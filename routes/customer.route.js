const express = require('express');
const router = express.Router();
const product_controller = require('../controllers/customer.controller');

// when inserting data for the first time
router.get('/allData', product_controller.allData); 
// when filtering data according to location
router.get('/filterData', product_controller.filterData);

module.exports = router;