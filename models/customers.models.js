const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CustomerSchema = new Schema({

    user_id: {
        type: Number,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true
    },
    location: {
        type: {
            type: String,
            enum: ['Point'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    }

}
);

CustomerSchema.index({ location: '2dsphere' });
// Export the model
module.exports = mongoose.model('Customer', CustomerSchema);