

const express = require('express');
const bodyParser = require('body-parser');
const schema = require('./models/customers.models')
const product = require('./routes/customer.route');
const errorHandling = require('./error_handling/errorhandling')
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = 'mongodb+srv://biz2credit:biz2credit@customersdata-jqbcx.mongodb.net/customersdata?retryWrites=true';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true })
mongoose.Promise = global.Promise;
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/customers', product);
app.use(errorHandling)

let port = 8000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
})